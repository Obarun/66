/* 
 * sconf.h
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#ifndef SCONF_H
#define SCONF_H


#include <66/enum.h>

#include <sys/types.h>
#include <stdint.h>

#include <oblibs/oblist.h>
#include <oblibs/stralist.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>
#include <skalibs/types.h>


typedef struct sv_common_s sv_common,*sv_common_ref ;
struct sv_common_s
{
	stralloc name ;
	stralloc description ;
	genalloc depends ;
	/**opts[0]->logger,opts[1]->data,opts[2]->env
	 * logger[1] enabled,logger[0] not enabled*/
	int opts[3] ;
	/**flags[0]->down,flags[1]->nosetsid
	 * down[1] enabled,down[0] not enabled*/
	int flags[2] ;
	/**0->no notification*/
	uint32_t notification ;
	/** array of uid_t
	 * the first element of the table
	 * is reserved to know the number of
	 * user set e.g user[0]=3->3 user*/
	uid_t user[256] ; 
	/**timeout[0]->kill,timeout[1]->finish
	* timeout[2]->up,timeout[3]->down
	* kill[0][X] enabled,kill[0][0] not enabled*/
	uint32_t timeout[4][UINT_FMT] ;
} ;
/**struct for run and finish file*/
typedef struct sv_exec_s sv_exec,*sv_exec_ref ;
struct sv_exec_s
{
	/**build=1->auto,build=2->custom*/
	int build ;
	uid_t runas ;
	stralloc shebang ;
	stralloc exec ;
} ;

typedef struct sv_execlog_s sv_execlog,*sv_execlog_ref ;
struct sv_execlog_s
{
	sv_exec run ;
	/**timeout[0]->kill,timeout[1]->finish
	 * kill[0][X] enabled,kill[0][0] not enabled*/
	uint timeout[2][UINT_FMT] ;
	stralloc destination ;
	uint32_t backup ;
	uint32_t maxsize ;
	/**timestamp=1->tai,timestamp=2->iso*/
	int timestamp ;
	/**only used for s6-rc type
	 * array of stralist*/
	genalloc consumer ; 
} ;

typedef struct sv_classic_s sv_classic,*sv_classic_ref ;
struct sv_classic_s
{
	sv_exec	run ;
	sv_exec finish ;
	sv_execlog log ;
} ;

typedef struct sv_longrun_s sv_longrun,*sv_longrun_ref ;
struct sv_longrun_s
{
	sv_exec run ;
	sv_exec finish ;
	sv_execlog log ;
	stralloc pipeline ;	
	genalloc producer ;
} ;
typedef struct sv_oneshot_s sv_oneshot,*sv_oneshot_ref ;
struct sv_oneshot_s
{
	sv_exec up ;
	sv_exec down ;
} ;

typedef union sv_type_u sv_type_t,*sv_type_t_ref ;
union sv_type_u
{
	sv_classic classic ;
	sv_longrun longrun ;
	sv_oneshot oneshot ;
} ;
typedef struct sv_alltype_s sv_alltype,*sv_alltype_ref ;
struct sv_alltype_s
{
	sv_type_t type ;
	sv_common common ;
	int itype ;/**int type =0>classic,1>bundle,2>longrun,3>oneshot*/
} ;

#define SV_COMMON_ZERO { STRALLOC_ZERO,\
						STRALLOC_ZERO,\
						GENALLOC_ZERO,\
						{ 0 },\
						{ 0 },\
						0, \
						{ 0 }, \
						{ { 0 } } }
#define SV_EXEC_ZERO { 	0 ,\
						0,\
						STRALLOC_ZERO,\
						STRALLOC_ZERO }
#define SV_EXECLOG_ZERO { SV_EXEC_ZERO,\
						{ { 0 } },\
						STRALLOC_ZERO,\
						0,\
						0,\
						0,\
						GENALLOC_ZERO }
#define SV_CLASSIC_ZERO { SV_EXEC_ZERO,\
						SV_EXEC_ZERO, \
						SV_EXECLOG_ZERO }
#define SV_LONGRUN_ZERO { SV_EXEC_ZERO,\
						SV_EXEC_ZERO,\
						SV_EXECLOG_ZERO,\
						STRALLOC_ZERO,\
						GENALLOC_ZERO }
#define SV_ONESHOT_ZERO { SV_EXEC_ZERO,\
						SV_EXEC_ZERO }
#define SV_ALLTYPE_ZERO { { SV_CLASSIC_ZERO } ,SV_COMMON_ZERO, 0 }
			
/** Search if the number of @sepstart and @sepend are equal
 * into @line over @lensearch of lenght
 * @Return 1 on success
 * @Return -1 if @sepstart>@sepend
 * @Return -2 if @sepstart<@sepend
 * @Return 0 if @sepstart && @sepend is zero*/
extern int scan_nbsep(char *line,int lensearch, char const sepstart, char const sepend) ;

/** Check if @septart exist before @sepend into @line
 * @Return the lenght on success
 * @Return -1 if @sepstart was not found or
 * if @sepstart is found after @sepend
 * @Return 0 if @sepend is not found*/
extern ssize_t get_sep_before (char const *line, char const sepstart, char const sepend) ;

/** Display the corresponding warning when @sepend and @sepstart are not equal */ 
extern void sep_err(int r,char const sepstart,char const sepend,char const *keyname) ;

/** Parse the value of a valid key included 
 * between @sepstart and @sepend into @ga form @pos.
 * Key is compared to each elements of @list. 
 * into @ga form @pos.
 * @Return 1 on success.
 * @Return -1 if the number of @sepstart and @sepend 
 * is not equal.
 * @Return -2 if an invalid line is found*/
extern int parse_sep (genalloc *ga, stralloc *sa, int idkey,key_description_t const *list,char const sepstart, char const sepend,int *pos) ;

/** Search for the next valid key on @ga from @pos
 * in @list with @sepend as character to find on the line
 * This function also scan for valid section
 * @Return n line from the next key or section
 * @Return 0 if a the next key or section is not found before
 * the end of the @ga
 * @Return -1 on other case*/
extern int get_nextval(genalloc *ga,int idkey,key_description_t const *list, int pos, char const sepend) ;

/** Scan @s to find '[' and ']' character
 * @Return n bytes to ']'
 * @Return 0 if '[' is not found
 * @Return -1 if ']' is not found*/ 
extern ssize_t scan_section(char const *s) ;

/** Search for a valid section on @s
 * and store the result in @sa
 * @Return 1 on success
 * @Return -4 for invalid section syntax
 * @Return -1 if it's not a section
 * @Return -2 if it's not a good section*/
extern int get_cleansection(stralloc *sa,char const *s) ;

/** Compare @key to all elements of @list
 * @Return the index of @key on @list table
 * @Return -1 on fail*/
extern ssize_t scan_keybyname(char const *key,key_description_t const *list) ;

/** Search in @s the '=' character
 * @Return n bytes to '='
 * @Return -1 if '=' is not found 
 * @Return -2 if '=' is the first character of the line*/
extern int scan_key(char const *s) ;

/** Search for a valid key at @s
 * remove all spaces around @s
 * @Return 1 on success
 *@Return -1 on fail*/
extern int get_cleankey(char *s) ;

extern void parse_err(int ierr,int idsec,int idkey) ;

extern int get_emptyval(genalloc *ga,char const *val,unsigned int idkey) ;

extern int get_svtype(genalloc *ga,int nbline) ;

/**lus->line,uint,slash*/	
extern int parse_lus(genalloc *ganocheck,char *line,unsigned int idkey) ;

extern int parse_common(sv_alltype *service,char *nocheck,unsigned int idsec, unsigned int idkey) ;

extern int parse_runfinish(sv_exec *exec,char *nocheck,unsigned int idsec,unsigned int idkey) ;

extern int parse_logger(sv_execlog *log,char *nocheck,unsigned int idsec,unsigned int idkey) ;

extern int parse_keystyle(sv_alltype *service,unsigned int svtype, genalloc *galine, unsigned int nbline) ;

extern int parse_sec(sv_alltype *service,unsigned int svtype,char *ganocheck,unsigned int idsec,unsigned int idkey) ;

extern int parse_bracket(genalloc *ganocheck, genalloc *galine,key_description_t const *list,unsigned int idkey,int *pos) ;



extern int sconf_parser(char const *s,sv_alltype *service) ;
#endif
