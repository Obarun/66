/* 
 * enum.h
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#ifndef ENUM_H
#define ENUM_H


#include <sys/types.h>

typedef enum key_description_section_e key_description_section_t;
enum key_description_section_e
{
	MAIN = 0,
	START,
	STOP,
	LOG,
	ENDOFSECTION
} ;
extern int const key_description_section_el ;

typedef enum key_description_key_e key_description_key_t ;
enum key_description_key_e
{
	TYPE = 0,
	NAME,
	DESCRIPTION,
	CONTENTS,
	DEPENDS,
	OPTIONS,
	NOTIFY,
	USER,
	PIPELINE,
	PRODUCER,
	CONSUMER,
	BUILD,
	FLAGS,
	RUNAS,
	SHEBANG,
	T_KILL,
	T_FINISH,
	T_UP,
	T_DOWN,
	EXEC,
	DESTINATION,
	BACKUP,
	MAXSIZE,
	TIMESTAMP,
	ENDOFKEY
} ;
extern int const key_description_key_el ;

typedef enum key_description_svtype_e key_description_svtype_t ;
enum key_description_svtype_e
{
	CLASSIC = 0 ,
	BUNDLE,
	LONGRUN,
	ONESHOT,
	ENDOFTYPE
} ;
extern int const key_description_svtype_el ;

typedef enum key_description_expected_e key_description_expected_t ;
enum key_description_expected_e
{
	LINE = 0 ,
	BRACKET,
	UINT,
	SLASH,
	ENDOFEXPECTED
} ;
extern int const key_description_expected_el ;

typedef enum key_description_opts_e key_description_opts_t ;
enum key_description_opts_e
{
	LOGGER = 0 ,
	DATA,
	ENV,
	ENDOFOPTS
} ;
extern int const key_description_opts_el ;

typedef enum key_description_flags_e key_description_flags_t ;
enum key_description_flags_e
{
	DOWN = 0 ,
	NOSETSID,
	ENDOFFLAGS
} ;
extern int const key_description_flags_el ;

typedef enum key_description_build_e key_description_build_t ;
enum key_description_build_e
{
	AUTO = 0 ,
	CUSTOM,
	ENDOFBUILD
} ;
extern int const key_description_build_el ;

typedef enum key_description_mandatory_e key_description_mandatory_t ;
enum key_description_mandatory_e
{
	NEED = 0 ,
	OPTS,
	ENDOFMANDATORY
} ;
extern int const key_description_mandatory_el ;

typedef enum key_description_timestamp_e key_description_timestamp_t ;
enum key_description_timestamp_e
{
	TAI = 0 ,
	ISO,
	ENDOFTIMESTAMP
} ;
extern int const key_description_timestamp_el ;

typedef struct key_description_s key_description_t ;
struct key_description_s
{
	char const *name ;
	key_description_expected_t const expected ;
	key_description_mandatory_t const mandatory ; 
} ;

typedef struct key_all_s key_all_t ;
struct key_all_s
{
	key_description_t const *list ;
} ;

static key_description_t const variable_list[] =
{
	{ .name = "type",  .expected = LINE, .mandatory = NEED },
	{ .name = "name", .expected = LINE, .mandatory = NEED },
	{ .name = "description", .expected = LINE, .mandatory = NEED },
	{ .name = "depends", .expected = BRACKET, .mandatory = OPTS  },
	{ .name = "options", .expected = BRACKET, .mandatory = OPTS },
	{ .name = "flags", .expected = BRACKET, .mandatory = OPTS },
	{ .name = "notify", .expected = UINT, .mandatory = OPTS },
	{ .name = "user", .expected = BRACKET, .mandatory = OPTS },
	{ .name = "timeout-finish", .expected = UINT, .mandatory = OPTS },
	{ .name = "timeout-kill", .expected = UINT, .mandatory = OPTS },
	{ .name = "timeout-up", .expected = UINT, .mandatory = OPTS },
	{ .name = "timeout-down", .expected = UINT, .mandatory = OPTS },
	{ .name = 0 } 
} ;

static key_description_t const startstop_section_list[] =
{
	{ .name = "build", .expected = LINE, .mandatory = NEED },
	{ .name = "runas", .expected = LINE, .mandatory = OPTS },
	{ .name = "shebang", .expected = SLASH, .mandatory = CUSTOM },
	{ .name = "execute", .expected = BRACKET, .mandatory = NEED },
	{ .name = 0 }
} ;

static key_description_t const logger_section_list[] =
{
	{ .name = "destination", .expected = SLASH, .mandatory = CUSTOM },
	{ .name = "build",  .expected = LINE, .mandatory = LOGGER },
	{ .name = "runas", .expected = LINE, .mandatory = OPTS },
	{ .name = "shebang", .expected = SLASH, .mandatory = CUSTOM },
	{ .name = "timeout-finish", .expected = UINT, .mandatory = OPTS },
	{ .name = "timeout-kill", .expected = UINT, .mandatory = OPTS },
	{ .name = "backup", .expected = UINT, .mandatory = OPTS },
	{ .name = "maxsize", .expected = UINT, .mandatory = OPTS },
	{ .name = "timestamp", .expected = LINE, .mandatory = OPTS },
	{ .name = "execute", .expected = BRACKET, .mandatory = CUSTOM },
	{ .name = 0 }
} ;

static int const total_list_el[4] = { 13, 5, 5, 11 } ;
static key_all_t const total_list[] =
{
	{ .list = variable_list },
	{ .list = startstop_section_list },
	{ .list = startstop_section_list },
	{ .list = logger_section_list },
	{ .list = 0 }
	
} ;
extern size_t const key_description_len ;

/** Compare @str with function @func on @key_el enum table
 * @Return the index of the table
 * @Return -1 on fail*/
extern ssize_t get_enumbyid(char const *str,char const *(*func)(),int key_el) ;

/**@Return the string of @section*/
extern char const *get_secbyid (key_description_section_t section) ;

/**@Return the string of the @key */ 
extern char const *get_keybyid(key_description_key_t key) ;

/**@Return the string of the @svtype */ 
extern char const *get_svtypebyid(key_description_svtype_t svtype) ;

/**@Return the string of @ex*/
extern char const *get_expectedbyid(key_description_expected_t ex) ;

/**@Return the string of the @opts*/
extern char const *get_optsbyid(key_description_opts_t opts) ;

/**@Return the string of the @flags*/
extern char const *get_flagsbyid(key_description_flags_t flags) ;

/**@Return the string of the @build*/
extern char const *get_buildbyid(key_description_build_t build) ;

/**@Return the string of the @man*/
extern char const *get_mandatorybyid(key_description_mandatory_t man) ;

/**@Return the string of the @time*/
extern char const *get_timestampbyid(key_description_timestamp_t time) ;

#endif
