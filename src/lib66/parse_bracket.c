/* 
 * parse_bracket.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <66/sconf.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>

int parse_bracket(genalloc *ganocheck, genalloc *galine,key_description_t const *list,unsigned int idkey,int *pos)
{
	int r ;
	stralloc bval = STRALLOC_ZERO ; /**value not checked*/
	
	char const sepstart = 0x28 ; /** '(' character */
	char const sepend = 0x29 ; /** ')' character */
	
	r = parse_sep(galine,&bval,idkey,list,sepstart,sepend,pos) ;
	if (r<0) return r ;
	if(!get_emptyval(ganocheck,bval.s,idkey)) return 0 ;
	stralloc_free(&bval) ;
	return 1 ;
}
