/* 
 * get_keyname.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <sys/types.h>

#include <66/sconf.h>
#include <oblibs/string.h>

ssize_t scan_keybyname(char const *key,key_description_t const *list)
{
	for(unsigned int i=0;list[i].name != 0;i++)
		if (obstr_equal(key,list[i].name)) return i ;
	
	return -1 ;
}

int scan_key(char const *s)
{
	ssize_t end ;
	end = get_sep_before(s,'=','\n') ;
	return 	(!end) ? -2 :
			(end<0) ? -1 :
			end ;
}

int get_cleankey(char *s)
{
	ssize_t end ;
	char const *k ;
	k = s ;
	end = scan_key(k)  ;
	if(end<0) return -1 ;
	s[end] = 0 ;
	obstr_trim(s,' ') ;
	return 1 ;
}
