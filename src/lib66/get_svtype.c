/* 
 * get_svtype.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <sys/types.h>

#include <66/sconf.h>

#include <oblibs/string.h>
#include <oblibs/error.h>
#include <oblibs/bytes.h>
#include <oblibs/stralist.h>

#include <skalibs/genalloc.h>

int get_svtype(genalloc *ga,int nbline)
{
	int r,pos ;
	size_t len ;
	int stype = -1 ;
	pos = 0 ;
	for(int i=0;i<nbline;i++)
		len += gaistrlen(ga,i) ;
	
	char buf[len] ;
	char *k = NULL ;
	char *v = NULL ;

	while(nbline>0)
	{
		r = get_wasted_line(gaistr(ga,pos)) ;
		if (!r)
		{
			nbline-- ; 
			pos++ ; 
			continue ; 
		}
		get_len_until(gaistr(ga,pos),'\n') ;
		stra_iecpytobuf(buf,ga,pos,pos) ;
		r = get_sep_before(buf,'=','\n') ;
		if(r<0)
		{ 
			byte_tozero(buf,gaistrlen(ga,pos)) ;
			nbline-- ;
			pos++ ; 
			continue ; 
		}
		k = buf ;
		v = buf ;
		obstr_sep(&v,"=") ;
		obstr_trim(k,' ') ;
		obstr_trim(v,' ') ;
		obstr_trim(v,'\n') ;
		if(obstr_equal(k,"type")){
			stype = get_enumbyid(v,get_svtypebyid,key_description_svtype_el) ;
			if(stype<0){
				stdout_msgw2x("unknow : type : ",v) ; 
				return -1 ; 
			}
			break ;
		}
		nbline--;
		pos++;
		byte_tozero(buf,gaistrlen(ga,pos)) ;
	}
	if(stype<0){
		stdout_msgw1x("unable to find key : type") ;
		return -1 ;
	}
	return stype ;
}
