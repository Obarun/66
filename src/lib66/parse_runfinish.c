/* 
 * parse_runfinish.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <66/sconf.h>

#include <oblibs/error.h>
#include <oblibs/types.h>
#include <oblibs/string.h>
#include <oblibs/stralist.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>


int parse_runfinish(sv_exec *exec,char *nocheck,unsigned int idsec,unsigned int idkey)
{
	int r ;

	genalloc gatmp = GENALLOC_ZERO ;

	switch(idkey){
		case BUILD:
			if(!exec->build){
			r = get_enumbyid(nocheck,get_buildbyid,key_description_build_el) ;
			if(r<0)
			{
				parse_err(0,idsec,BUILD) ;
				goto err;
			}
			exec->build = r+1 ;
			}else{
				parse_err(1,idsec,BUILD) ;
				goto err;
			}					
			break ;
		case RUNAS:
			r = scan_uid(nocheck,&exec->runas) ;
			if (!r) 
			{
				parse_err(0,idsec,RUNAS) ;
				goto err ;
			}
			break ;
		case SHEBANG:
			if(!exec->shebang.len){
				r = scan_absopath(nocheck) ;
				if(r<0)
				{
					parse_err(4,idsec,SHEBANG) ;
					goto err ;
				}
				if(!stralloc_cats(&exec->shebang,nocheck)) die_nomem("parse_runfinish:stralloc:SHEBANG") ;
				if(!stralloc_0(&exec->shebang)) die_nomem("parse_runfinish:stralloc:SHEBANG:0") ;
			}else{
				parse_err(1,idsec,SHEBANG) ;
				goto err ;
			}
			break ;
		case EXEC:
			if(!exec->exec.len){
				if(!stralloc_cats(&exec->exec,nocheck)) die_nomem("parse_runfinish:stralloc:EXEC") ;
				if(!stralloc_0(&exec->exec)) die_nomem("parse_runfinish:stralloc:EXEC:0") ;
			}else{
				parse_err(1,idsec,EXEC) ;
				goto err ;
			}
			break ;
		default:
			stdout_msgw4x("ignoring key : ",get_keybyid(idkey)," : on section : ",get_secbyid(idsec)) ;
			break ;
		}

	genalloc_free(stralist,&gatmp) ;
	return 1 ;

	err:
		genalloc_free(stralist,&gatmp) ;
		return 0 ;
}
