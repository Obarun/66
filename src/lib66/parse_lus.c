/* 
 * parse_lus.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <66/sconf.h>

#include <oblibs/string.h>

#include <skalibs/genalloc.h>
	
int parse_lus(genalloc *ganocheck,char *line,unsigned int idkey)
{
	char *val = line ;
	obstr_sep(&val,"=") ;
	/** only pass trough obstr_trim if the key is different
	 *  of description to avoid removing the space*/
	if( idkey != 2 ) obstr_trim(val,' ') ;
	obstr_trim(val,'\n') ;
	if (!get_emptyval(ganocheck,val,idkey)) return 0 ;
	
	return 1 ;
}
