/* 
 * parse_sep.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include <66/sconf.h>

#include <oblibs/memory.h>
#include <oblibs/error.h>
#include <oblibs/stralist.h>
#include <oblibs/string.h>
#include <oblibs/bytes.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>

int parse_sep (genalloc *ga, stralloc *sa, int idkey,key_description_t const *list,char const sepstart, char const sepend,int *pos)
{
	ssize_t len, end, galen ;
	int r, rs, ierr ;
	int npos ; /**next position*/
	unsigned int bpos ;
	int nbpos ;
	char key[15] ; /**timeout-finish +1*/
	
	byte_tozero(key,15) ;
	r = rs = len = end = galen = nbpos = 0 ;
	ierr = 1 ;
	npos = bpos = *pos ;
	
	for(unsigned int i = 0 ;i<genalloc_len(stralist,ga);i++)
		galen += gaistrlen(ga,i) ;
	
	char val[galen] ;
	byte_tozero(val,galen) ;

	/**parse the key*/
	r = scan_isspace(gaistr(ga,(*pos))) ;
	end = get_sep_before(gaistr(ga,(*pos)),'=','\n') ;
	if( end < 0 )
	{
		ierr = -1 ; 
		goto err ; 
	}
	memcpy(key,gaistr(ga,(*pos))+r,end-r);
	r = get_cleankey(key) ;
	
	/**look for @sepstart and store value on @val*/
	end = get_sep_before(gaistr(ga,(*pos)),sepstart,'\n') ;
	if( end < 0 )
	{
		rs = end ;
		ierr = -5 ;
		goto err ; 
	}
	memcpy(val,gaistr(ga,(*pos))+end,galen);
	
	end = get_sep_before(gaistr(ga,(*pos)),sepend,'\n') ;
	
	/**complex case @sepend and @sepstart are not on the same line*/
	if( end < 0 ){ 
		
		nbpos = get_nextval(ga,idkey,list,(*pos),sepend) ;
		if(nbpos<0)	return nbpos ;
		/**end of string*/
		if (!nbpos){
			npos = genalloc_len(stralist,ga) ;
			r = 0 ;
			/**remove empty line*/
			while (!r){
				r = get_wasted_line(gaistr(ga,npos)) ;
				if(!r) npos-- ;
			}
			r = -1 ;
			/**search for the firt @sepend from the end of the string*/
			while(r<0){
				r = get_sep_before(gaistr(ga,npos),sepend,'\n') ;
				if(r<0)	npos-- ;
				if(npos == *pos)
				{
					ierr = -2 ;
					goto err ;
				} 
			}
			/** special case for key execute*/
			r = get_len_until(gaistr(ga,npos),sepend) ;
			if(r<0)
			{
				ierr = -1 ; 
				goto err ; 
			}
			stra_iecpytobuf(val,ga,(*pos)+1,npos);
			/** check if the number of @sepstart and @sepend are equal*/
			rs = scan_nbsep(val,strlen(val),sepstart,sepend) ;
			if(rs == -1 || rs == -2 || !rs)
			{
				ierr = -5 ; 
				goto err ; 
			}
			r = get_rlen_until(val,sepend,strlen(val)) ;
			if(r<0)
			{
				ierr = -5 ; 
				goto err ;
			}
			if (!stralloc_catb(sa,val+1,strlen(val)-(strlen(val)-r)-1)) die_nomem("get_between_sep:stralloc complex") ;
			if(!stralloc_0(sa)) die_nomem("get_between_sep:stralloc_0 complex");
			*pos = *pos+(npos-*pos) ;
			return ierr ;
		}
		
		
	}
	/**Search the first @sepend from the end of the string*/
	r = get_rlen_until(gaistr(ga,(nbpos+*pos)),sepend,gaistrlen(ga,(nbpos+*pos))) ;
	if(r<0)
	{
		rs = -1 ;
		ierr = -5 ; 
		goto err ;
	}
	/**pos+1->first line was already copied
	 * pos+npos+1->*/
	stra_iecpytobuf(val,ga,(*pos)+1,(*pos)+nbpos);
	rs = scan_nbsep(val,strlen(val),sepstart,sepend) ;
	if(rs == -1 || rs == -2 || !rs)
	{
		ierr = -5 ;
		goto err ;
	}
	r = get_rlen_until(val,sepend,strlen(val)) ;
	if (!stralloc_catb(sa,val+1,strlen(val)-(strlen(val)-r)-1)) die_nomem("get_between_sep:stralloc simple") ;
	if(!stralloc_0(sa)) die_nomem("get_between_sep:stralloc_0 simple") ;
	/**Set @pos to the end of the value of the current key*/
	*pos = nbpos+bpos ;
	
	return ierr ;
	
	err: 
		*pos = nbpos+bpos ;
		if(ierr == -5){
			sep_err(rs,sepstart,sepend,get_keybyid(idkey)) ;		
			ierr = -1 ;
		}
		return ierr ;
}
