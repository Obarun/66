/* 
 * parse_sec.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <66/sconf.h>

#include <oblibs/error.h>

#include <skalibs/genalloc.h>

int parse_sec(sv_alltype *service,unsigned int svtype,char *ganocheck,unsigned int idsec,unsigned int idkey)
{
	int r ;
	r = 0 ;
	switch(idsec){
			case MAIN:
				r = parse_common(service,ganocheck, idsec, idkey) ;
				if(!r) return 0 ;
				break;
			case START:
				if(svtype == CLASSIC )
					r = parse_runfinish(&service->type.classic.run,ganocheck,idsec,idkey) ;
				if(svtype == LONGRUN )
					r = parse_runfinish(&service->type.longrun.run,ganocheck,idsec,idkey) ;
				if(svtype == ONESHOT )
					r = parse_runfinish(&service->type.oneshot.up,ganocheck,idsec,idkey) ;
				if(svtype == BUNDLE ){
					r = 1 ;
					//stdout_msgw1x("ignoring section : start : for type : bundle") ;
				}
				if(!r) return 0 ;
				break ;
			case STOP:
				if(svtype == CLASSIC )
					r = parse_runfinish(&service->type.classic.finish,ganocheck,idsec,idkey) ;
				if(svtype == LONGRUN )
					r = parse_runfinish(&service->type.longrun.finish,ganocheck,idsec,idkey) ;
				if(svtype == ONESHOT )
					r = parse_runfinish(&service->type.oneshot.down,ganocheck,idsec,idkey) ;
				if(svtype == BUNDLE ){
					r = 1 ;
					//stdout_msgw1x("ignoring section : stop : for type : bundle") ;
				}
				if(!r) return 0 ;
				break ;
			case LOG:
				if(svtype == CLASSIC )
					r = parse_logger(&service->type.classic.log,ganocheck,idsec,idkey) ;
				if(svtype == LONGRUN )
					r = parse_logger(&service->type.longrun.log,ganocheck,idsec,idkey) ;
				if(svtype == BUNDLE || svtype == ONESHOT){
					r = 1 ;
					//stdout_msgw2x("ignoring section : logger : for type : ",get_svtypebyid(svtype)) ;
				}
				if(!r) return 0 ;
				break ;
			default:
				stdout_msgw2x("unknow section : ",get_secbyid(idsec)) ;
				break ;
	}
	return 1 ;
}
