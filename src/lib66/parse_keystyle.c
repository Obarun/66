/* 
 * parse_keystyle.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <sys/types.h>
#include <string.h>

#include <66/sconf.h>

#include <oblibs/string.h>
#include <oblibs/stralist.h>
#include <oblibs/error.h>
#include <oblibs/bytes.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>
#include <skalibs/types.h>

#include <stdio.h>
int parse_keystyle(sv_alltype *service,unsigned int svtype, genalloc *galine, unsigned int nbline)
{
	int r,rk,pos,bpos,ierr ;
	unsigned int idkey,idsec ;
	ssize_t end ;
	size_t slen = 0;
	char spos[UINT_FMT] ; /**only used on error msg*/
	
	for(unsigned int i=0;i<nbline;i++)
		slen += gaistrlen(galine,i) ;
	char key[slen] ;
	
	key_all_t const *list = total_list ;
	stralloc bval = STRALLOC_ZERO ; /**tmp value not checked*/
	stralloc section = STRALLOC_ZERO ;
	genalloc ganocheck = GENALLOC_ZERO ;

	r = pos = bpos = ierr = 0 ;

	while(nbline>0)
	{	
		/**skip empty line*/
		r = get_wasted_line(gaistr(galine,pos)) ;
		if (!r)
		{ 
			nbline-- ; 
			pos++ ; 
			bpos = pos ; 
			continue ; 
		}
		r = scan_isspace(gaistr(galine,pos)) ;

		end = get_len_until(gaistr(galine,pos),'\n') ;
	
		/** erase old string on @key coming from the last loop*/
		byte_tozero(key,slen) ;
		memcpy(key,gaistr(galine,pos)+r,end-r);
	
		/**check for section first*/
		if(scan_section(key)){
			section = stralloc_zero ;
			r = get_cleansection(&section,key) ;
			if( r == -4) { ierr = -4 ; goto err ; }
			if( r == -2) { ierr = -3 ; goto err ; }
			if( r == -1) { ierr = -2 ; goto err ; }
			/** get the id of the section*/
			idsec = get_enumbyid(section.s,get_secbyid,key_description_section_el) ;
			nbline--;
			pos++;
			bpos = pos ;
			continue ;
		}
		
		/** search for a valid key*/
		r = get_cleankey(key) ;
		if(r<0)
		{
			ierr = -2 ;
			goto err ; 
		}
		/**check if @key exist on section*/
		rk = scan_keybyname(key,list[idsec].list) ;
		if(rk<0)
		{
			ierr = 0 ; 
			goto err ;
		}
		/** get the index of the @key*/
		idkey = get_enumbyid(key,get_keybyid,key_description_key_el) ;
		
		switch(list[idsec].list[rk].expected){
					
			case LINE:
				r = parse_lus(&ganocheck,gaistr(galine,pos),idkey) ;
				if(!r)
				{	
					ierr = -5 ;
					goto err ;
				}	
				break ;
			case BRACKET:
				r = parse_bracket(&ganocheck,galine,list[idsec].list,idkey,&pos) ;
				if(r<0)
				{ 
					ierr = r ; 
					goto err ;
				}
				if(!r)
				{
					ierr = 0 ;
					goto err ;
				}
				/** set the nbline according to the jump of pos
				* made on parse_sep sub-function*/
				if (bpos != pos) nbline -= pos-bpos ;
				bpos = pos ;
				break ;
			case UINT:
				r = parse_lus(&ganocheck,gaistr(galine,pos),idkey) ;
				if(!r)
				{
					ierr = -5 ; 
					goto err ;
				}
				break ;
			case SLASH:
				r = parse_lus(&ganocheck,gaistr(galine,pos),idkey) ;
				if(!r)
				{ 
					ierr = -5 ;
					goto err ; 
				}
				break ;
			default:
				stdout_msgw2x("unknow format : ",get_expectedbyid(list[idsec].list[rk].expected)) ;
				ierr = -5 ; 
				goto err ;
		}
		/**store the nochecked value on struct of the service*/
		r = parse_sec(service,svtype,gastr(&ganocheck),idsec,idkey) ;
		if(!r)
		{ 
			ierr = -5 ;
			goto err ;
		}
		ganocheck = genalloc_zero ;
		bval = stralloc_zero ;
		nbline-- ;
		pos++ ;
		bpos = pos ;
	}
	
	stralloc_free(&bval) ;
	stralloc_free(&section) ;
	genalloc_free(stralist,&ganocheck) ;
	return 1 ;
	
	err:
		spos[uint_fmt(spos,pos)] = 0 ;/** allow to display the int*/
		if(ierr == -4) stdout_msgw2x("invalid syntax for section : ", section.s) ;
		if(ierr == -3) stdout_msgw2x("invalid section : ",section.s) ;
		if(ierr == -2) stdout_msgw2x("invalid line number : ",spos) ;
		if(ierr == -1) stdout_msgw2x("invalid syntax for key : ",key) ;
		if(!ierr) stdout_msgw4x("invalid key : ",key," : for section ",section.s) ;
		
		stralloc_free(&bval) ;
		stralloc_free(&section) ;
		genalloc_free(stralist,&ganocheck) ;
		return 0 ;
}
