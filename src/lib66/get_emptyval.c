/* 
 * get_emptyval.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <66/sconf.h>

#include <oblibs/string.h>
#include <oblibs/error.h>
#include <oblibs/stralist.h>

#include <skalibs/genalloc.h>

int get_emptyval(genalloc *ga,char const *val,unsigned int idkey)
{
	int r ;
	r = get_wasted_line(val) ;
	if(!r)
	{
		stdout_msgw2x("empty value for key : ",get_keybyid(idkey)) ;
		return 0 ;
	}
	stra_add(ga,val) ;
	return 1 ;
}
