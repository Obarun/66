/* 
 * get_enum.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <sys/types.h>
#include <stdio.h>
#include <66/sconf.h>
#include <oblibs/string.h>

char const *get_secbyid (key_description_section_t section)
{
	return 	(section == MAIN) ? "main" :
			(section == START) ? "start" :
			(section == STOP) ? "stop" :
			(section == LOG) ? "logger" :
			"unknow" ;
}
int const key_description_section_el = ENDOFSECTION - MAIN ;

char const *get_keybyid(key_description_key_t key)
{
	
	return	(key == TYPE ) ? "type" :
			(key == NAME ) ? "name" :
			(key == DESCRIPTION ) ? "description" :
			(key == CONTENTS ) ? "contents" :
			(key == DEPENDS ) ? "depends" :
			(key == OPTIONS ) ? "options" :
			(key == NOTIFY ) ? "notify" :
			(key == USER ) ? "user" :
			(key == PIPELINE ) ? "pipeline" :
			(key == PRODUCER ) ? "producer" :
			(key == CONSUMER ) ? "consumer" :
			(key == BUILD ) ? "build" :
			(key == FLAGS ) ? "flags" :
			(key == RUNAS ) ? "runas" :
			(key == SHEBANG ) ? "shebang" :
			(key == T_FINISH ) ? "timeout-finish" :
			(key == T_KILL ) ? "timeout-kill" :
			(key == T_UP ) ? "timeout-up" :
			(key == T_DOWN ) ? "timeout-down" :
			(key == EXEC ) ? "execute" :
			(key == DESTINATION ) ? "destination" :
			(key == BACKUP ) ? "backup" :
			(key == MAXSIZE ) ? "maxsize" :
			(key == TIMESTAMP ) ? "timestamp" :			
			"unknow" ;
}
int const key_description_key_el = ENDOFKEY - TYPE ;	

char const *get_svtypebyid(key_description_svtype_t svtype)
{
	return 	(svtype == CLASSIC ) ? "classic" :
			(svtype == BUNDLE ) ? "bundle" :
			(svtype == LONGRUN ) ? "longrun" :
			(svtype == ONESHOT ) ? "oneshot" :
			"unknow" ;
}
int const key_description_svtype_el = ENDOFTYPE - CLASSIC ;

char const *get_expectedbyid(key_description_expected_t ex)
{
	return 	(ex == LINE ) ? "line" :
			(ex == BRACKET ) ? "bracket" :
			(ex == UINT ) ? "uint" :
			(ex == SLASH ) ? "slash" :
			"unknow" ;
}
int const key_description_expected_el = ENDOFEXPECTED - LINE ;

char const *get_optsbyid(key_description_opts_t opts)
{
	return 	(opts == LOGGER ) ? "logger" :
			(opts == DATA ) ? "data" :
			(opts == ENV ) ? "env" :
			"unknow" ;
}
int const key_description_opts_el = ENDOFOPTS - LOGGER ;	

char const *get_flagsbyid(key_description_flags_t flags)
{
	return 	(flags == DOWN ) ? "down" :
			(flags == NOSETSID ) ? "nosetsid" :
			"unknow" ;
}
int const key_description_flags_el = ENDOFFLAGS - DOWN ;	

char const *get_buildbyid(key_description_build_t build)
{
	return 	(build == AUTO ) ? "auto" :
			(build == CUSTOM ) ? "custom" :
			"unknow" ;
}
int const key_description_build_el = ENDOFBUILD - AUTO ;

char const *get_mandatorybyid(key_description_mandatory_t man)
{
	return 	(man == NEED ) ? "need" :
			(man == OPTS ) ? "opts" :
			"unknow" ;
}
int const key_description_mandatory_el = ENDOFMANDATORY - NEED ;

char const *get_timestampbyid(key_description_timestamp_t time)
{
	return 	(time == TAI ) ? "tai" :
			(time == ISO ) ? "iso" :
			"unknow" ;
}
int const key_description_timestamp_el = ENDOFTIMESTAMP - TAI ;

ssize_t get_enumbyid(char const *str,char const *(*func)(),int key_el)
{
	int i  = 0 ;

	for (;i<key_el;i++)
		if(obstr_equal(str,func(i))) return i ;
	
	return -1 ;
}
