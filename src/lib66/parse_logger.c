/* 
 * parse_logger.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <stdint.h>

#include <66/sconf.h>

#include <oblibs/error.h>
#include <oblibs/types.h>
#include <oblibs/string.h>
#include <oblibs/stralist.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>
#include <skalibs/types.h>

int parse_logger(sv_execlog *log,char *nocheck,unsigned int idsec,unsigned int idkey)
{
	int r ;

	genalloc gatmp = GENALLOC_ZERO ;

	switch(idkey){
		case BUILD:
			r = parse_runfinish(&log->run,nocheck,idsec,idkey) ;
			if(!r) goto err ;
			break ;
		case RUNAS:
			r = parse_runfinish(&log->run,nocheck,idsec,idkey) ;
			if(!r) goto err ;
			break ;
		case SHEBANG:
			r = parse_runfinish(&log->run,nocheck,idsec,idkey) ;
			if(!r) goto err ;
			break ;
		case EXEC:
			r = parse_runfinish(&log->run,nocheck,idsec,idkey) ;
			if(!r) goto err ;
			break ;
		case T_KILL:
			r = scan_timeout(nocheck,(uint32_t *)log->timeout,0) ;
			if(r<0){
				parse_err(3,idsec,T_KILL) ;
				goto err ;
			}
			if(!r){
				parse_err(1,idsec,T_KILL) ;
				goto err ;
			}
			break ;
		case T_FINISH:
			r = scan_timeout(nocheck,(uint32_t *)log->timeout,1) ;
			if(r<0){
				parse_err(3,idsec,T_FINISH) ;
				goto err ;
			}
			if(!r){
				parse_err(1,idsec,T_FINISH) ;
				goto err ;
			}
			break ;
		case DESTINATION:
			if(!log->destination.len){
				r = scan_absopath(nocheck) ;
				if(r<0)
				{
					parse_err(4,idsec,DESTINATION) ;
					goto err ;
				}
				if(!stralloc_cats(&log->destination,nocheck)) die_nomem("parse_logger:stralloc:DESTINATION") ;
				if(!stralloc_0(&log->destination)) die_nomem("parse_logger:stralloc:DESTINATION:0") ;
			}else{
				parse_err(1,idsec,DESTINATION) ;
				goto err ;
			}
			break ;
		case BACKUP:
			if(!log->backup){
				clean_val(nocheck,&gatmp) ;
				if(!scan_uint32(gastr(&gatmp)))
				{
					parse_err(3,idsec,BACKUP) ;
					goto err ;
				}
				uint_scan(gastr(&gatmp),&log->backup) ;
			}else{
				parse_err(1,idsec,BACKUP) ;
				goto err ;
			}	
			break ;
		case MAXSIZE:
			if(!log->maxsize){
				if(!scan_uint32(nocheck))
				{
					parse_err(3,idsec,MAXSIZE) ;
					goto err ;
				}
				uint_scan(nocheck,&log->maxsize) ;
			}else{
				parse_err(1,idsec,MAXSIZE) ;
				goto err ;
			}
			break ;
		case TIMESTAMP:
			if(!log->timestamp){
				r = get_enumbyid(nocheck,get_timestampbyid,key_description_timestamp_el) ;
				if(r<0)
				{
					parse_err(0,idsec,TIMESTAMP) ;
					goto err ;
				}
				log->timestamp = r ;
			}else{
				parse_err(1,idsec,TIMESTAMP) ;
				goto err ;
			}
			break ;
		default:
			stdout_msgw4x("ignoring key : ",get_keybyid(idkey)," : on section : ",get_secbyid(idsec)) ;
			break ;
	}
	genalloc_free(stralist,&gatmp) ;
	return 1 ;
	
	err:
		genalloc_free(stralist,&gatmp) ;
		return 0 ;
}
