/* 
 * scan_nbsep.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <stddef.h>
#include <stdio.h> // to remove
#include <66/sconf.h>
#include <oblibs/bytes.h>

int scan_nbsep(char *line,int lensearch, char const sepstart, char const sepend)
{
	size_t start, end ;
	start = byte_tosearch(line,lensearch,sepstart) ;
	end = byte_tosearch(line,lensearch,sepend) ;
	if (start > end ) return -1 ; /**unmatched ( */
	if (start < end ) return -2 ; /**unmatched ) */
	if ((!start) && (!end)) return 0 ; /** find nothing*/
	return 1 ;
}
