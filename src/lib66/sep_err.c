/* 
 * sep_err.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
 
#include <oblibs/error.h>

void sep_err(int r,char const sepstart,char const sepend,char const *keyname)
{
	if (r == -1) stdout_msgw4x("unmatched ", &sepstart," at key : ",keyname) ;
	if (r == -2) stdout_msgw4x("unmatched ", &sepend," at key : ",keyname) ;
	if (!r) stdout_msgw2x("bad syntax at key : ",keyname) ;
}
