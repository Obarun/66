/* 
 * get_secname.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <sys/types.h>
#include <stdio.h>
#include <66/sconf.h>
#include <oblibs/string.h>
#include <oblibs/error.h>

#include <skalibs/stralloc.h>

ssize_t scan_section(char const *s)
{
	ssize_t end;
	end = 0 ;
	if(s[0] == '[') end = get_len_until(s,']') ;
	return end<0 ? -1 : end ;
}

int get_cleansection(stralloc *sa,char const *s)
{
	ssize_t end ;
	int r, ierr ;
	r = ierr = 0 ;
	end = scan_section(s) ;
	
	if(end>0){
		if (!stralloc_catb(sa,s+1,end - 1 )) die_nomem("get_cleansection") ;
	}else{
		if (!stralloc_catb(sa,s+1,strlen(s))) die_nomem("get_cleansection") ;
	}
	if (!stralloc_0(sa)) die_nomem("get_cleansection:stralloc_0") ;
	r = get_enumbyid(sa->s,get_secbyid,key_description_section_el) ;
	if(end<0) return -4 ;
	if(!end) return -1 ;
	if(r<0) return -2 ;
	return 1 ;
}
