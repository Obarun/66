/* 
 * parse_err.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <66/sconf.h>

#include <oblibs/error.h>

void parse_err(int ierr,int idsec,int idkey)
{
	switch(ierr)
	{
		case 0: 
			stdout_msgw4x("invalid value for key : ",get_keybyid(idkey)," : in section : ",get_secbyid(idsec)) ;
			break ;
		case 1:
			stdout_msgw4x("multiple definition of key : ",get_keybyid(idkey)," : in section : ",get_secbyid(idsec)) ;
			break ;
		case 2:
			stdout_msgw4x("same value for key : ",get_keybyid(idkey)," : in section : ",get_secbyid(idsec)) ;
			break ;
		case 3:
			stdout_msgw4x("key : ",get_keybyid(idkey)," : must be an integrer value in section : ",get_secbyid(idsec)) ;
			break ;
		case 4:
			stdout_msgw4x("key : ",get_keybyid(idkey)," : must be an absolute path in section : ",get_secbyid(idsec)) ;
			break ;
		case 5:
			stdout_msgw4x("key : ",get_keybyid(idkey)," : must be set in section : ",get_secbyid(idsec)) ;
			break ;
		default:
			break ;
	}
}
