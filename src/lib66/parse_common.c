/* 
 * parse_common.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <stdint.h>

#include <66/sconf.h>

#include <oblibs/error.h>
#include <oblibs/types.h>
#include <oblibs/string.h>
#include <oblibs/stralist.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>
#include <skalibs/types.h>

int parse_common(sv_alltype *service,char *nocheck,unsigned int idsec, unsigned int idkey)
{
	int r ;
	unsigned int i ;
	genalloc gatmp = GENALLOC_ZERO ;
	r = i = 0 ;
	
	switch(idkey){
		case TYPE:
			break;
		case NAME:
			if(!service->common.name.len){
				if(!stralloc_cats(&service->common.name,nocheck)) die_nomem("parse_common:stralloc:NAME") ;
				if(!stralloc_0(&service->common.name)) die_nomem("parse_common:stralloc:NAME:0") ;
			}else{
				parse_err(1,idsec,NAME) ;
				goto err;
			}
			break ;
		case DESCRIPTION:
			if(!service->common.description.len){
				if(!stralloc_cats(&service->common.description,nocheck)) die_nomem("parse_common:stralloc:DESCRIPTION") ; 
				if(!stralloc_0(&service->common.description)) die_nomem("parse_common:stralloc:DESCRIPTION:0") ; 
			}else{
				parse_err(1,idsec,DESCRIPTION) ;
				goto err;
			}
			break ;
		case OPTIONS:
			if(!service->common.opts[r]){
				clean_val(nocheck,&gatmp) ;
				for (i=0;i<genalloc_len(stralist,&gatmp);i++)
				{
					r = get_enumbyid(gaistr(&gatmp,i),get_optsbyid,key_description_opts_el) ;
					if(r<0) 
					{
						parse_err(0,idsec,OPTIONS) ;
						goto err;
					}
					service->common.opts[r] = 1 ;/**0 means not enabled*/
				}
			}else{
				parse_err(2,idsec,OPTIONS) ;
				goto err;
			}
			break ;
		case FLAGS:
			if(!service->common.flags[r]){
				clean_val(nocheck,&gatmp) ;
				for (i=0;i<genalloc_len(stralist,&gatmp);i++)
				{
					r = get_enumbyid(gaistr(&gatmp,i),get_flagsbyid,key_description_flags_el) ;
					if(r<0) 
					{
						parse_err(0,idsec,FLAGS) ;
						goto err;
					}
					service->common.flags[r] = 1 ;/**0 means not enabled*/				
				}
			}else{
				parse_err(2,idsec,FLAGS) ;
				goto err;
			}
			break ;
		case USER:
			r = scan_uidlist(nocheck,(uid_t *)service->common.user) ;
			if (!r) 
			{
				parse_err(0,idsec,USER) ;
				goto err ;
			}
			break ;
		case DEPENDS:
			clean_val(nocheck,&service->common.depends) ;
			break ;
		case NOTIFY:
			if(!service->common.notification){
				clean_val(nocheck,&gatmp) ;
				if(!scan_uint32(gastr(&gatmp)))
				{
					parse_err(3,idsec,NOTIFY) ;
					goto err ;
				}
				uint32_scan(gastr(&gatmp),&service->common.notification) ;
			}else{
				parse_err(1,idsec,NOTIFY) ;
				goto err;
			}
			break ;
		case T_KILL:
			r = scan_timeout(nocheck,(uint32_t *)service->common.timeout,0) ;
			if(r<0){
				parse_err(3,idsec,T_KILL) ;
				goto err ;
			}
			if(!r){
				parse_err(1,idsec,T_KILL) ;
				goto err ;
			}
			break ;
		case T_FINISH:
			r = scan_timeout(nocheck,(uint32_t *)service->common.timeout,1) ;
			if(r<0){
				parse_err(3,idsec,T_FINISH) ;
				goto err ;
			}
			if(!r){
				parse_err(1,idsec,T_FINISH) ;
				goto err ;
			}
			break ;
		case T_UP:
			r = scan_timeout(nocheck,(uint32_t *)service->common.timeout,2) ;
			if(r<0){
				parse_err(3,idsec,T_UP) ;
				goto err ;
			}
			if(!r){
				parse_err(1,idsec,T_UP) ;
				goto err ;
			}
			break ;
		case T_DOWN:
			r = scan_timeout(nocheck,(uint32_t *)service->common.timeout,3) ;
			if(r<0){
				parse_err(3,idsec,T_DOWN) ;
				goto err ;
			}
			if(!r){
				parse_err(1,idsec,T_DOWN) ;
				goto err ;
			}
			break ;
		default:
			stdout_msgw4x("ignoring key : ",get_keybyid(idkey)," : on section : ",get_secbyid(idsec)) ;
			break ;
	}
	
	genalloc_free(stralist,&gatmp) ;
	return 1 ;
	
	err:
		genalloc_free(stralist,&gatmp) ;
		return 0 ;
}
