/* 
 * get_nextval.c
 * 
 * Copyright (c) 2018 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution and at https://github.com/Obarun/66/LICENSE
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */
 
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include <66/sconf.h>

#include <oblibs/memory.h>
#include <oblibs/error.h>
#include <oblibs/stralist.h>
#include <oblibs/string.h>
#include <oblibs/bytes.h>

#include <skalibs/stralloc.h>
#include <skalibs/genalloc.h>

int get_nextval(genalloc *ga,int idkey,key_description_t const *list, int pos, char const sepend)
{
	int r, bpos,ri,nbline, ierr,posline ;
	size_t len ;
	ssize_t end ;
	stralloc tmp = STRALLOC_ZERO ;
	char key[15] ;/**timeout-finish +1 */
	byte_tozero(key,15) ;
	
	r = len = end = ierr = ri = nbline = posline= 0 ;
	bpos = pos ;
	nbline = genalloc_len(stralist,ga) - pos ;

	while(posline < nbline){
		/**skip empty line*/
		while(!r){
			pos++ ;
			posline++ ;
			r = get_wasted_line(gaistr(ga,pos)) ;
		}
		ri = scan_isspace(gaistr(ga,pos)) ;
		r = scan_key(gaistr(ga,pos)) ;
				
		/** = was not found, check for section*/
		if(r<0){
			r = get_cleansection(&tmp,gaistr(ga,pos)+ri) ;
			tmp = stralloc_zero ;
			/** good section*/
			if(r>0){
				pos-- ;
				r = 0 ;
				while(!r){
					r = get_wasted_line(gaistr(ga,pos)) ;
					if(!r) pos-- ;
				}
				r = -1 ;
				/**Search for the first @sepend from the end */
				while(r<0){
					r = get_sep_before(gaistr(ga,pos),sepend,'\n') ;
					if(r<0){
						pos-- ;
					}
					if(pos == bpos) { ierr = -1 ; goto err ; } 
				}
				break ;
			}
			/**not a good section continue to the next line*/

			posline++ ;
			pos++ ;
			/**end of file, not a real error but pass through
			 * err to freed the stralloc*/
			if (posline == nbline){ ierr = 0 ; goto err ; } 
			
			continue ;
		}
		/**parse the key and compare it to the key on @list*/
		
		memcpy(key,gaistr(ga,pos),r) ;
		obstr_trim(key,' ') ;
		key[r] = '\0' ;
		
		r = scan_keybyname(key,list) ;
		
		/** invalid key, check if the key currently parsed is exec,
		 * if not the founded key is not valid,warn and exit*/
		if(r<0) { 
			if(idkey != 19)
			{
				stdout_msgw2x("invalid key : ", key) ;
				ierr = -2 ;
				goto err;
			}
			pos++ ; 
			posline++ ;
			if (posline == nbline)
			{
				ierr = 0 ;
				goto err ;
			}
			continue ; 
		}
		/** return to the previous line and check for wasted line*/
		pos-- ;
		r = -1 ;
		while(r<0)
		{
			r = get_wasted_line(gaistr(ga,pos)) ;
			if(!r)
			{ 
				pos-- ;
				r = -1 ; 
			}
		}
		/** search for the first @sepend from the end*/
		r = get_sep_before(gaistr(ga,pos),sepend,'\n') ;
		while(r<0){
			r = get_sep_before(gaistr(ga,pos),sepend,'\n') ;
			if(r<0) pos-- ;
			/** sepend was not found before return to the start of the search*/
			if(pos == bpos)
			{
				ierr = -1 ;
				goto err ;
			} 
		}
		break ;
	}

	stralloc_free(&tmp) ;
	return (pos - bpos == 0) ? pos : pos - bpos ;
	
	err:
		stralloc_free(&tmp) ;
		/** return -1 for bad syntax,
		 *  -2 for invalid key,
		 *  0 for end of file*/
		return ierr ;
}
